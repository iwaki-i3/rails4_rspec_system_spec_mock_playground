FROM ruby:2.3.3-alpine

RUN apk add --update --no-cache --virtual=build-dependencies \
      build-base \
      git \
      libxml2-dev \
      libxslt-dev \
      mariadb-dev \
      nodejs \
      ruby-dev \
      tzdata \
      yaml-dev \
      zlib-dev
