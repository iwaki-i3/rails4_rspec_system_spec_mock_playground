class Admin::UsersController < SecureApplicationController
  def index
    @users = User.all
  end

  def new
    @user = User.new
  end

  def create
    @user = User.new(params.require(:user).permit(:name, :email))
    if @user.save
      redirect_to action: :index
    else
      render :new
    end
  end
end
