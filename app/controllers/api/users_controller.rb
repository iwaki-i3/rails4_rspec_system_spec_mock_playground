class Api::UsersController < ApplicationController
  # GET /api/users/:name
  def show
    user = User.find_by!(name: params[:name])
    render json: user.attributes.slice("name", "email")
  rescue ActiveRecord::RecordNotFound
    render json: {}, status: :not_found
  end
end
