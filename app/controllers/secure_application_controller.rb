class SecureApplicationController < ApplicationController
  class Unauthorized < StandardError ; end

  before_action do
    if current_user_id.blank?
      raise Unauthorized
    end
  end

  rescue_from Unauthorized do
    render plain: '401 Unauthorized', status: 401
  end
end