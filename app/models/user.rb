class User < ActiveRecord::Base
  validates :name,
    presence: true,
    uniqueness: true

  validates :email,
    presence: true

  def name_with_email
    "#{name} <#{email}>"
  end
end
