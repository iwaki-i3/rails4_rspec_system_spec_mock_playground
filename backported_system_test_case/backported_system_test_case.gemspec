# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'backported_system_test_case/version'

Gem::Specification.new do |spec|
  spec.name          = "backported_system_test_case"
  spec.version       = BackportedSystemTestCase::VERSION
  spec.authors       = ["YusukeIwaki"]
  spec.email         = ["yusuke.iwaki@example.com"]

  spec.summary       = %q{Backport of SystemTestCase from Rails 5.2}
  spec.description   = %q{Backport of SystemTestCase from Rails 5.2}
  spec.homepage      = "https://example.com/"

  spec.files         = `git ls-files -z`.split("\x0").reject do |f|
    f.match(%r{^(test|spec|features)/})
  end
  spec.bindir        = "exe"
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]

  spec.add_dependency "capybara", "~> 3.15.1"
  spec.add_dependency "rails", "~> 4.2.6"
  spec.add_development_dependency "bundler", "~> 1.14"
  spec.add_development_dependency "rake", "~> 10.0"
end
