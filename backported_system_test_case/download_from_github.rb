#!/bin/sh

BRANCH=5-2-stable
BASE_URL=https://raw.githubusercontent.com/rails/rails/$BRANCH/actionpack/lib
OUT_DIR=lib

rm -rf $OUT_DIR/action_dispatch/system_test_case.rb $OUT_DIR/action_dispatch/system_testing
mkdir -p $OUT_DIR/action_dispatch/system_testing/test_helpers

wget $BASE_URL/action_dispatch/system_test_case.rb \
   -O $OUT_DIR/action_dispatch/system_test_case.rb
wget $BASE_URL/action_dispatch/system_testing/browser.rb \
   -O $OUT_DIR/action_dispatch/system_testing/browser.rb
wget $BASE_URL/action_dispatch/system_testing/server.rb \
   -O $OUT_DIR/action_dispatch/system_testing/server.rb
wget $BASE_URL/action_dispatch/system_testing/driver.rb \
   -O $OUT_DIR/action_dispatch/system_testing/driver.rb
wget $BASE_URL/action_dispatch/system_testing/test_helpers/screenshot_helper.rb \
   -O $OUT_DIR/action_dispatch/system_testing/test_helpers/screenshot_helper.rb
wget $BASE_URL/action_dispatch/system_testing/test_helpers/setup_and_teardown.rb \
   -O $OUT_DIR/action_dispatch/system_testing/test_helpers/setup_and_teardown.rb
wget $BASE_URL/action_dispatch/system_testing/test_helpers/undef_methods.rb \
   -O $OUT_DIR/action_dispatch/system_testing/test_helpers/undef_methods.rb
