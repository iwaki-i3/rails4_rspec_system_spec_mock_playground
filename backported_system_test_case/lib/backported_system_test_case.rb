require "backported_system_test_case/version"
if Rails::VERSION::MAJOR >= 5
  # Rails 5.1以降であれば、標準で入っている仕組みなので、消し外し忘れていたら例外を出して気付けるようにする。
  raise 'backported_system_test_caseのGemを削除し忘れてない？'
end

require "action_dispatch/system_test_case"
require "action_dispatch/integration_test/behavior"
if Rails.env.test?
  # ActiveRecordの挙動変更が必要なのは、テスト時に setup_fixturesするときだけなので、
  # 影響範囲をおさえるため RAILS_ENV=test のときだけパッチをあてる。
  require "backported_system_test_case/active_record_monkey_patching"
end
