# SystemTestCaseで、
#
# before {
#   # ログインしたことにする
#   user = create(:user)
#   allow_any_instance_of(ApplicationController).to receive(:current_user).and_return(user)
# }
#
# のようにFactoryBotなどを用いた試験をするには、
# https://github.com/rails/rails/pull/28083/files （Rails 5.1から入った）を入れる必要がある。
#
# これを入れることで、DBコネクションがRSpecとRailsサーバーで共有されるため、
# RSpecプロセス側でuserを作ったものがそのままRailsサーバー側からも見えるようになる。

# https://github.com/rails/rails/pull/28083 はRails 5.0ベースの変更。
# Rails 4 では
# https://github.com/rails/rails/blob/4-2-stable/activerecord/lib/active_record/connection_adapters/abstract/connection_pool.rb
# current_connection_id をキーにconnectionの払い出しを行うようになっている。
# そのため、lock_thread = true/false 時に、そのコネクションIDを覚えておくようにすればよい。
module BackportedSystemTestCase::ActiveRecordConnectionPoolPatching
  def lock_thread=(lock_thread)
    if lock_thread
      @locked_thread_id = current_connection_id
    else
      @locked_thread_id = nil
    end
  end  

  def current_connection_id
    @locked_thread_id || super
  end
end
ActiveRecord::ConnectionAdapters::ConnectionPool.prepend(BackportedSystemTestCase::ActiveRecordConnectionPoolPatching)

# connection.pool.lock_thread = true/false を絶妙な位置に差し込む必要があるが、
# 無理ゲーなので、メソッド全部をコピーしたものに差し込む。
module BackportedSystemTestCase::SystemTestingPatching
  # copied from https://github.com/rails/rails/blob/4-2-stable/activerecord/lib/active_record/fixtures.rb
  def setup_fixtures(config = ActiveRecord::Base)
    if pre_loaded_fixtures && !use_transactional_fixtures
      raise RuntimeError, 'pre_loaded_fixtures requires use_transactional_fixtures'
    end

    @fixture_cache = {}
    @fixture_connections = []
    @@already_loaded_fixtures ||= {}

    # Load fixtures once and begin transaction.
    if run_in_transaction?
      if @@already_loaded_fixtures[self.class]
        @loaded_fixtures = @@already_loaded_fixtures[self.class]
      else
        @loaded_fixtures = load_fixtures(config)
        @@already_loaded_fixtures[self.class] = @loaded_fixtures
      end
      @fixture_connections = enlist_fixture_connections
      @fixture_connections.each do |connection|
        connection.begin_transaction joinable: false
        connection.pool.lock_thread = true ## 追加。 ref: https://github.com/rails/rails/pull/28083/files
      end
    # Load fixtures for every test.
    else
      ActiveRecord::FixtureSet.reset_cache
      @@already_loaded_fixtures[self.class] = nil
      @loaded_fixtures = load_fixtures(config)
    end

    # Instantiate fixtures for every test if requested.
    instantiate_fixtures if use_instantiated_fixtures
  end

  def teardown_fixtures
    # Rollback changes if a transaction is active.
    if run_in_transaction?
      @fixture_connections.each do |connection|
        connection.rollback_transaction if connection.transaction_open?
        connection.pool.lock_thread = false ## 追加。 ref: https://github.com/rails/rails/pull/28083/files
      end
      @fixture_connections.clear
    else
      ActiveRecord::FixtureSet.reset_cache
    end

    ActiveRecord::Base.clear_active_connections!
  end
end
ActionDispatch::SystemTesting::TestHelpers::SetupAndTeardown.prepend(BackportedSystemTestCase::SystemTestingPatching)
