require 'rails_helper'

RSpec.describe User do
  describe '#name_with_email' do
    subject { user.name_with_email }
    let(:user) { User.create!(name: name, email: email) }

    context 'nameとemailが正常のとき' do
      let(:name) { "YusukeIwaki" }
      let(:email) { "yusuke.iwaki@example.com" }
      it { is_expected.to eq("YusukeIwaki <yusuke.iwaki@example.com>") }
    end
  end
end