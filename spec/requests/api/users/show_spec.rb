require 'rails_helper'

RSpec.describe 'GET /api/users/:name', type: :request do
  subject { get "/api/users/#{name}" }

  context '存在するユーザを指定した場合' do
    let(:name) { User.create!(name: "hoge", email: "hoge@example.com").name }
    it {
      subject
      expect(response).to have_http_status(200)
      expect(response.body).to be_json_eql("hoge".to_json).at_path("name")
      expect(response.body).to be_json_eql("hoge@example.com".to_json).at_path("email")
    }
  end

  context '存在しないユーザを指定した場合' do
    let(:name) { "hoge" }
    it {
      subject
      expect(response).to have_http_status(404)
    }
  end
end