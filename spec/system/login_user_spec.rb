require 'rails_helper'

RSpec.describe 'ログインユーザ表示' do
  before {
    driven_by :selenium,
      using: :remote,
      options: {
        url: "http://#{ENV["SELENIUM_HOSTNAME"]}:4444/wd/hub",
        desired_capabilities: :chrome
      }
    Capybara.server = :webrick
    rails_ip = Socket.ip_address_list.detect{|addr| addr.ipv4_private?}.ip_address
    Capybara.server_host = rails_ip
    Capybara.app_host = "http://#{rails_ip}"
  }

  context '未ログイン' do
    it '/userにアクセスすると、ユーザ名が見えること' do
      visit '/user'
      find('a.btn-primary') # ajaxで入れ込まれる要素が現れるまで待つ
      expect(page).to have_content("Not logged in")
    end
  end

  context 'ログイン済み' do
    before {
      user = User.create!(name: "its_me", email: "me@example.com")
      allow_any_instance_of(ApplicationController).to receive(:current_user_id).and_return(user.id)
    }

    it '/userにアクセスすると、ユーザ名が見えること' do
      visit '/user'
      find('a.btn-primary') # ajaxで入れ込まれる要素が現れるまで待つ
      expect(page).to have_content("its_me")
    end
  end
end